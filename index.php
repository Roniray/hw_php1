<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>HW1_korabel</title>
      <style>
        body {
          display: block;
          background-color: #383838;
        }
        header {
          display: flex;
          flex-direction: row;
          justify-content: space-evenly;
        }
        h1 {
          align-self: center;
          display: flex;
          margin-right: 200px;
          color: white;
          font-size: 28px;
        }
        main {
          margin-top: 20px;
          text-align: center; 
          color: white;
        }
        p {
          color: white;
          font-size: 16px;
        }
        footer {
          margin-top: 25px;
          text-align: center;
          color: white;
        }
      </style>
  </head>
  <body>
      <header>
          <img width="10%" src="https://mospolytech.ru/local/templates/main/dist/img/logos/mospolytech-logo-white.svg" alt="logo">
          <h1> Задание «Hello, World!» </h1>
      </header>
      <main>
          <?="<p>PHP in mospolytech!</p>"?>
      </main>
      <footer>
          <p>Сверстать веб-страницу с динамическим контентом. Загрузить код на удалённый репозиторий. Залить на хостинг.</p>
      </footer>
  </body>
</html>
